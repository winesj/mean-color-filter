[![builds.sr.ht status](https://builds.sr.ht/~jackwines/mean-color-filter.svg)](https://builds.sr.ht/~jackwines/mean-color-filter?)

This project uses cluster recognition on a sample of 300 pixels in an image to reduce the number of colors in said image.

### usage

```
mean-color-filter image.png cutFactor # replace cutFactor with a number between 0 and 1.8ish
```

### examples

![](sierra.jpg)
![](sierraResult.png)
![](landscape.jpeg)
![](landscapeResult.png)

### to build:

make sure you have [nix](nixos.org/nix) installed.
```
nix-build release.nix
```
`cabal new-build` or `cabal v1-build`gt might also work in a pinch.
