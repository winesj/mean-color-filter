module Main where
import qualified Graphics.Image as Img
import qualified Data.Vector.Unboxed as Vec
import qualified Graphics.Image.Interface as Int
import qualified Graphics.Image.ColorSpace as CS
import qualified Graphics.Image.Processing as P
import qualified Data.Clustering.Hierarchical as C
import qualified System.Environment as Env
import qualified System.Random as R
import qualified Data.Function as F
import Data.List


chunksOf :: Int -> [e] -> [[e]]
chunksOf n [] = []
chunksOf n ls = splita : chunksOf n splitb
    where
        (splita, splitb) = splitAt n ls

distance' :: (Int, CS.Pixel CS.RGB Double) -> (Int, CS.Pixel CS.RGB Double) -> C.Distance
distance' (_, a) (_, b) = distance a b

distance (CS.PixelRGB hue sat int) (CS.PixelRGB hue' sat' int')
    = vecLen [(hue - hue'), (sat - sat'), (int - int')]
    where
        vecLen = sqrt . sum . map (** 2)

addPx (CS.PixelRGB hue sat int) (CS.PixelRGB hue' sat' int') = CS.PixelRGB (hue + hue') (sat + sat') (int + int') 

meanColor :: [CS.Pixel CS.RGB Double] -> CS.Pixel CS.RGB Double
meanColor s = CS.PixelRGB ((hue sum') / l) ((sat sum') / l) ((int sum') / l)
    where
        sum' = (foldl1 addPx s)

        l :: Double
        l = fromIntegral . length $ s

hue (CS.PixelRGB hue sat int) = hue
sat (CS.PixelRGB hue sat int) = sat
int (CS.PixelRGB hue sat int) = int


cutDendro cut dendro = map C.elements . C.cutAt dendro $ cut
-- getLvl (C.Branch _ d1 d2) 1 = [C.elements d1, C.elements d2]
-- getLvl (C.Branch x d1 d2) levels 
--     | (length . C.elements $ (C.Branch x d1 d2)) < 10 = [C.elements $ C.Branch x d1 d2]
--     | otherwise = getLvl d1 (pred levels) ++ getLvl d2 (pred levels)
-- getLvl (C.Leaf a) _ = [[a]]


changeToMeans ps = zip (map fst ps) (repeat mean')
    where
        mean' = meanColor . map snd $ ps

sample :: (Vec.Unbox a) => R.StdGen -> Int -> Vec.Vector a -> [a]
sample gen n xs = map (Vec.unsafeIndex xs) rands
    where
        rands :: [Int]
        rands = take n . R.randomRs (0, Vec.length xs) $ gen

reconstructImg :: Int -> [[(Int, CS.Pixel CS.RGB Double)]] -> Img.Image Img.VU CS.RGB Double
reconstructImg colNum ps = Img.fromLists . chunksOf colNum . map snd . sortOn fst . concatMap changeToMeans $ ps

clustersFromSample :: R.StdGen -> Int -> Img.Image Img.VU CS.RGB Double -> C.Dendrogram (Img.Pixel CS.RGB Double)
clustersFromSample gen n img = makeDendrogram sampled distance
    where
        sampled :: [Img.Pixel CS.RGB Double]
        sampled = sample gen n $ Int.toVector img

nonClusterImage :: Img.Image Img.VU CS.RGB Double -> Double -> Img.Image Img.VU CS.RGB Double
nonClusterImage img cut' = reconstructImg (Img.cols img) . cutDendro cut' . makeDendrogram pixels $ distance'
    where
        pixels = zip [0..] . concat . Img.toLists $ img

minimumOn :: Ord b => (a -> b) -> [a] -> a
minimumOn f = minimumBy (compare `F.on` f)

reconstructClusterImage :: Img.Image Img.VU CS.RGB Double -> [[Img.Pixel CS.RGB Double]] -> Img.Image Img.VU CS.RGB Double
reconstructClusterImage img clusters = Img.map (\x -> minimumOn (distance x) clusterMeans) img
    where
        clusterMeans :: [Img.Pixel CS.RGB Double]
        clusterMeans = map meanColor clusters

sampleClusterImage gen n img cut = reconstructClusterImage img . cutDendro cut $ clustersFromSample gen n img

makeDendrogram = C.dendrogram C.CLINK

sqImg :: Img.Image Img.VU CS.RGB Double -> Img.Image Img.VU CS.RGB Double
sqImg = Img.map (fmap (**2))

sqrtImg :: Img.Image Img.VU CS.RGB Double -> Img.Image Img.VU CS.RGB Double
sqrtImg = Img.map (fmap sqrt)

main :: IO ()
main = do
    -- parse command line args
    (arg : cut : []) <- Env.getArgs
    let cut' = read cut :: Double
    img <- sqImg <$> Img.readImageRGB Img.VU arg
    -- let reconstructed = nonClusterImage img cut'
    gen <- R.getStdGen
    let img' = sampleClusterImage gen 300 img cut'
    Img.writeImage "result.png" $ sqrtImg img'
